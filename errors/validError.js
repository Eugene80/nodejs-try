class validError extends Error {
    constructor(message) {
        super(message); 
        this.name = "validError";
    }
}
module.exports = validError
